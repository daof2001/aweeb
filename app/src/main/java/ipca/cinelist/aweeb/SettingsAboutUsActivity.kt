package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView

class SettingsAboutUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_expand_details3)

        /*BUTTONS*/
        val messageButton = findViewById<ImageView>(R.id.message_button8)
        val searchButton = findViewById<ImageView>(R.id.search_button8)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button8)
        val userAvi = findViewById<CircleImageView>(R.id.userIc8)

        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users").child(user?.uid.toString())

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var currentUser = snapshot.getValue(User ::class.java)

                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvi.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@SettingsAboutUsActivity).load(currentUser?.imageURL).into(userAvi)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        /*ALL UI BUttons*/
        userAvi.setOnClickListener{
            val intent = Intent(this, UserProfileActivity :: class.java)
            startActivity(intent)
        }

        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
        }

        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }
}