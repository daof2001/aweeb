package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.*
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView

class SettingsChangePasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_expand_details)

        /*BUTTONS*/
        val messageButton = findViewById<ImageView>(R.id.message_button6)
        val searchButton = findViewById<ImageView>(R.id.search_button6)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button6)
        val userAvi = findViewById<CircleImageView>(R.id.userIc6)
        val confirmButton = findViewById<Button>(R.id.button_ConfirmPassword)

        //variables
        val sentEmail = findViewById<EditText>(R.id.email)

        val firebaseAuth = FirebaseAuth.getInstance()

        confirmButton.setOnClickListener {
            val email = sentEmail.text.toString()

            if (TextUtils.isEmpty(email))
                Toast.makeText(this, "You need to insert a valid email", Toast.LENGTH_SHORT).show()
            else
                firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener( this@SettingsChangePasswordActivity) { task ->
                    if (task.isSuccessful){
                        Toast.makeText(this, "Please check you inbox", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, SignInActivity::class.java)
                        startActivity(intent)
                    }
                    else{
                        val error = task.exception!!.message
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
                    }


                }
        }

        val user = firebaseAuth.currentUser
        val reference = Firebase.database.getReference("Users").child(user?.uid.toString())

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var currentUser = snapshot.getValue(User ::class.java)

                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvi.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@SettingsChangePasswordActivity).load(currentUser?.imageURL).into(userAvi)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        /*ALL UI BUttons*/
        userAvi.setOnClickListener{
            val intent = Intent(this, UserProfileActivity :: class.java)
            startActivity(intent)
        }

        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
        }

        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }
}