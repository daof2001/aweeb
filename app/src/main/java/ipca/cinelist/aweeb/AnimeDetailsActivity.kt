package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase

class AnimeDetailsActivity : AppCompatActivity() {

    private lateinit var referenceFavorite : DatabaseReference
    private lateinit var referenceSeen : DatabaseReference
    private lateinit var referenceRating : DatabaseReference

    var validReviews : MutableList<Review> = ArrayList()

    companion object{
        const val TITLE = "title"
        const val NUMEPISODES = "numEpisodes"
        const val AVERAGERATING = "averageRating"
        const val USERRATING = "userRating"
        const val IMAGEURL = "imageURL"
        const val ID = "malID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anime_details)

        val user = Firebase.auth.currentUser!!

        val title = intent.getStringExtra(TITLE)
        val numEpisodes = intent.getStringExtra(NUMEPISODES)
        val averageRating = intent.getStringExtra(AVERAGERATING)
        var userRating = intent.getStringExtra(USERRATING)
        val imageURL = intent.getStringExtra(IMAGEURL)
        val id = intent.getStringExtra(ID)

        val anime = Anime(id, title, averageRating, userRating, numEpisodes, imageURL)

        val textViewTitle = findViewById<TextView>(R.id.textViewAnimeTitle)
        val textViewNumEpisodes = findViewById<TextView>(R.id.textViewAnimeEpisodes)
        val textViewAverageRating = findViewById<TextView>(R.id.textViewAnimeAverageRating)
        val textViewUserRating = findViewById<TextView>(R.id.textViewAnimeUserRating)
        val imageViewAnimeDetail = findViewById<ImageView>(R.id.imageViewAnimeDetail)
        val favoriteButton = findViewById<ImageView>(R.id.favoriteButton)
        val reviewButton = findViewById<Button>(R.id.buttonWriteReview)

        val seenButton = findViewById<ImageView>(R.id.seenButton)

        Glide.with(this).load(imageURL).into(imageViewAnimeDetail)

        referenceRating = FirebaseDatabase.getInstance().getReference("Reviews")


        referenceRating.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (child in snapshot.children){
                    val review = child.getValue(Review::class.java)

                    if (review?.userID.equals(user.uid) && review?.animeURL!! == imageURL)
                        validReviews.add(review)
                }
                if (validReviews.size > 0){
                    validReviews.sortByDescending { it -> it.score }
                    userRating = validReviews[0].score
                }
                runOnUiThread {
                    textViewUserRating.text = userRating.toString()
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })

        textViewTitle.text = title
        textViewNumEpisodes.text = numEpisodes.toString()
        textViewAverageRating.text = averageRating.toString()



        referenceFavorite = FirebaseDatabase.getInstance().getReference("Users").child(user.uid).child("favoriteAnimes").child(title.toString())
        referenceSeen = FirebaseDatabase.getInstance().getReference("Users").child(user.uid).child("seenAnimes").child(title.toString())

        favoriteButton.setOnClickListener {
            referenceFavorite.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (!snapshot.hasChild("title")) {
                        referenceFavorite.setValue(anime)
                            .addOnCompleteListener(this@AnimeDetailsActivity) { task ->
                                if (task.isSuccessful)
                                    Toast.makeText(
                                        this@AnimeDetailsActivity,
                                        "Anime successfully added to your favorites",
                                        Toast.LENGTH_SHORT
                                    ).show()
                            }
                    } else {
                        Toast.makeText(
                            this@AnimeDetailsActivity,
                            "You have already added this anime to your favorites",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                }
            })
        }

        seenButton.setOnClickListener{
            referenceSeen.addListenerForSingleValueEvent(object: ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (!snapshot.hasChild("title")) {
                        referenceSeen.setValue(anime)
                            .addOnCompleteListener(this@AnimeDetailsActivity) { task ->
                                if (task.isSuccessful)
                                    Toast.makeText(this@AnimeDetailsActivity,"Anime successfully added to your watched animes",Toast.LENGTH_SHORT).show()
                            }
                    }
                    else{
                        Toast.makeText(this@AnimeDetailsActivity,"You have already added this anime to your watched animes", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                }

            })
        }

        reviewButton.setOnClickListener{
            if (validReviews.size == 0){
                val intent = Intent(this, ReviewDetailsActivity :: class.java)
                intent.putExtra(ReviewDetailsActivity.IMAGEURL, imageURL)
                intent.putExtra(ReviewDetailsActivity.ANIMEID, id)

                startActivity(intent)
            }
            else
                Toast.makeText(this, "You already submitted a review for this anime!", Toast.LENGTH_SHORT).show()
        }
    }

}


