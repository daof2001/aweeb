package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView

class PostActivity : AppCompatActivity() {

    var posts : MutableList<Review> = ArrayList()
    var postsAdapter = PostAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        val messageButton = findViewById<ImageView>(R.id.message_button5)
        val settingsButton = findViewById<ImageView>(R.id.settings_button5)
        val searchButton = findViewById<ImageView>(R.id.search_button5)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button5)
        val userAvatar = findViewById<CircleImageView>(R.id.userIc5)
        val username = findViewById<TextView>(R.id.usernameText)

        //variables
        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users").child(user?.uid.toString())
        val referenceReviews = Firebase.database.getReference("Reviews")

        referenceReviews.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                posts.clear()

                for (child in snapshot.children) {
                    val post = child.getValue(Review::class.java)

                    post?.let { posts.add(it) }
                    posts.reverse()
                }
                runOnUiThread{
                    postsAdapter.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var currentUser = snapshot.getValue(User ::class.java)
                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvatar.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@PostActivity).load(currentUser?.imageURL).into(userAvatar)
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })

        //ALL BUTTON ACTIONS
        userAvatar.setOnClickListener{
            val intent = Intent(this, UserProfileActivity::class.java)
            startActivity(intent)
            finish()
        }
        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity::class.java)
            startActivity(intent)
            finish()
        }
        //settings activity
        settingsButton.setOnClickListener{
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
            finish()
        }
        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity::class.java)
            startActivity(intent)
            finish()
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
            finish()
        }

        val listViewPosts = findViewById<ListView>(R.id.postListView)
        listViewPosts.adapter = postsAdapter
    }

    inner class PostAdapter : BaseAdapter(){
        override fun getCount(): Int {
            return posts.size
        }

        override fun getItem(position: Int): Any {
            return posts[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.row_post, parent, false)

            val reviewedAnimeImage = rowView.findViewById<ImageView>(R.id.reviewedAnimeImage)
            val review = rowView.findViewById<TextView>(R.id.reviewedAnimeText)
            val score = rowView.findViewById<TextView>(R.id.reviewScoreText)
            val username = rowView.findViewById<TextView>(R.id.usernameText)

            review.text = posts[position].text
            score.text = "Score: ${posts[position].score}"

            val reference = FirebaseDatabase.getInstance().getReference("Users").child(posts[position].userID.toString())

            reference.addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    var currentUser = snapshot.getValue(User::class.java)

                    //so it displays the users's info on his profile
                    username.setText(currentUser!!.username)
                }

                override fun onCancelled(error: DatabaseError) {
                }
            })

            if ((posts[position].animeURL?:"").contains("http")){
                Backend.getBitmapFromUrl(posts[position].animeURL!!){
                    reviewedAnimeImage.setImageBitmap(it)
                }
            }

            return rowView
        }

    }

}