package ipca.cinelist.aweeb

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class UserRegisterActivity : AppCompatActivity() {

    private lateinit var auth : FirebaseAuth
    private lateinit var reference : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_register)

        val usernameReg = findViewById<EditText>(R.id.usernameReg)
        val confirmButton = findViewById<Button>(R.id.confirm_button)

        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser


        confirmButton.setOnClickListener{
            val profileUpdates = userProfileChangeRequest {

                if (TextUtils.isEmpty(usernameReg.text.toString()))
                    Toast.makeText(this@UserRegisterActivity, "Please enter a valid username", Toast.LENGTH_SHORT).show()
                else if (usernameReg.text.toString().length >= 16)
                    Toast.makeText(this@UserRegisterActivity, "Your username is going to break the app", Toast.LENGTH_SHORT).show()
                else
                {
                    displayName = usernameReg.text.toString()

                    reference = FirebaseDatabase.getInstance().getReference("Users").child(user!!.uid).child("username")
                    reference.setValue(displayName)
                }
            }

            user!!.updateProfile(profileUpdates)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, UserProfileActivity::class.java)
                        startActivity(intent)
                    }
                }
        }

    }
}