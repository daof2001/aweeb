package ipca.cinelist.aweeb

import android.app.ProgressDialog
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.MimeTypeMap
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import de.hdodenhof.circleimageview.CircleImageView

class UserProfileActivity : AppCompatActivity() {

    final var IMAGE_REQUEST : Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        val username = findViewById<TextView>(R.id.userID)
        val descriptiveBio = findViewById<EditText>(R.id.userBio)

        /*Buttons*/
        //browsing the app buttons
        val messageButton = findViewById<ImageView>(R.id.message_button)
        val settingsButton = findViewById<ImageView>(R.id.settings_button)
        val searchButton = findViewById<ImageView>(R.id.search_button)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button)

        //user related buttons
        val expandAnimeDetails = findViewById<Button>(R.id.watched_animes_button)
        val expandSeriesDetails = findViewById<Button>(R.id.watched_series_button)
        val expandReviewDetails = findViewById<Button>(R.id.button_expandReviews)
        val expandFavoritesDetails = findViewById<TextView>(R.id.button_expandFavorites)

        //user avatar
        val userAvatar = findViewById<CircleImageView>(R.id.userAvatar)
        val userAvi = findViewById<CircleImageView>(R.id.userIc)

        val storageRef : StorageReference
        val uri : Uri
        val storageTask : StorageTask<UploadTask.TaskSnapshot>
        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users").child(user!!.uid)

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var currentUser = snapshot.getValue(User::class.java)

                //so it displays the users's info on his profile
                username.setText(currentUser!!.username)
                descriptiveBio.setText(currentUser.bio)

                if(currentUser.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvatar.setImageResource(R.mipmap.hm_round)
                    userAvi.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@UserProfileActivity).load(currentUser.imageURL).into(userAvatar)
                    Glide.with(this@UserProfileActivity).load(currentUser.imageURL).into(userAvi)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        //ALL BUTTON ACTIONS
        /*User action specific buttons*/

        userAvi.setOnClickListener{
            openImage()
        }
        //opens the activity related to the user's watched animes
        expandAnimeDetails.setOnClickListener{
            val intent = Intent(this, WatchedAnimeActivity::class.java)
            startActivity(intent)
        }

        //opens the activity related to the user's watched series
        expandSeriesDetails.setOnClickListener{
            val intent = Intent(this, SeriesDetailsActivity::class.java)
            startActivity(intent)
        }

        //opens the activity related to the user's written reviews
        expandReviewDetails.setOnClickListener {
            val intent = Intent(this, UserReviewActivity::class.java)
            startActivity(intent)
        }

        //opens the activity related to the user's favorites
        expandFavoritesDetails.setOnClickListener {
            val intent = Intent(this, FavoritesDetailsActivity::class.java)
            startActivity(intent)
        }

        /*Button actions related to App browsing and Activity Change*/
        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //settings activity
        settingsButton.setOnClickListener{
            val intent = Intent(this, SettingsActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity ::class.java)
            startActivity(intent)
            finish()
        }

    }

    private fun openImage() {
        val intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(intent, IMAGE_REQUEST)
    }

     fun getFileExtension(uri : Uri): String {
        val contentResolver : ContentResolver = contentResolver
        val mimeType : MimeTypeMap = MimeTypeMap.getSingleton()

        return mimeType.getExtensionFromMimeType(contentResolver.getType(uri)).toString()
    }
    private fun uploadImage(){
        //val pd : ProgressDialog
    }
}