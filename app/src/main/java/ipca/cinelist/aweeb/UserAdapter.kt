package ipca.cinelist.aweeb

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import ipca.cinelist.aweeb.UserAdapter.ViewHolder

 class UserAdapter : RecyclerView.Adapter<ViewHolder>{
        var contex : Context
        var userList : MutableList<User> = ArrayList()

    constructor(contex: Context, userList: MutableList<User>) : super() {
        this.contex = contex
        this.userList = userList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(contex).inflate(R.layout.users_display_item, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user : User = userList.get(position)
        holder.username.setText(user.username)

        if(user.imageURL.equals("default")){
            holder.profile_image.setImageResource(R.mipmap.hm_round)
        }
        else{
            Glide.with(contex).load(user.imageURL).into(holder.profile_image)
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(contex, ChatActivity :: class.java)
            intent.putExtra("userid", user.id)
            contex.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }


     class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val username = itemView.findViewById<TextView>(R.id.friend_username)
            val profile_image = itemView.findViewById<CircleImageView>(R.id.profile_image)
    }
}