package ipca.cinelist.aweeb

import android.media.Image
import org.json.JSONObject
import java.util.*

class Anime {
    var id : String? = null
    var title : String? = null
    var averageRating : String? = null
    var userRating : String? = null
    var episodesCount : String? = null
    var mainPicture : String? = null
    var airing : String? = null

    constructor(
        id: String?,
        title: String?,
        averageRating: String?,
        userRating: String?,
        episodesCount: String?,
        mainPicture: String?,
    ) {
        this.id = id
        this.title = title
        this.averageRating = averageRating
        this.userRating = userRating
        this.episodesCount = episodesCount
        this.mainPicture = mainPicture
    }

    constructor(){
        averageRating = "0"
        userRating = "0"
        airing = "false"
    }

    companion object {
        fun fromJson(jsonObject: JSONObject) : Anime{
            val anime = Anime()
            anime.title = jsonObject.getString("title")
            anime.mainPicture = jsonObject.getString("image_url")
            anime.episodesCount = jsonObject.getString("episodes")
            anime.averageRating = jsonObject.getString("score")
            anime.id = jsonObject.getString("mal_id")

            return anime
        }
    }

}