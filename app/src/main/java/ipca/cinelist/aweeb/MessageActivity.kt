package ipca.cinelist.aweeb

import android.content.Intent
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TableLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView

class MessageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        /*BUTTONS*/
        val messageButton = findViewById<ImageView>(R.id.message_button4)
        val settingsButton = findViewById<ImageView>(R.id.settings_button4)
        val searchButton = findViewById<ImageView>(R.id.search_button4)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button4)
        val userAvatar = findViewById<CircleImageView>(R.id.userIc4)
        val tabLayout = findViewById<TabLayout>(R.id.tableLayout)
        val view = findViewById<ViewPager>(R.id.view_pager)

        //variables
        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users").child(user?.uid.toString())

        val fragmentManager : FragmentManager = supportFragmentManager
        val messageChatFragment = MessageChatFragment()
        val messageUsersFragment = MessageUsersFragment()

        val viewPageAdapter = ViewPageAdapter(fragmentManager)
        viewPageAdapter.addFragment(messageChatFragment, "Chats")
        viewPageAdapter.addFragment(messageUsersFragment, "Users")
        view.setAdapter(viewPageAdapter)

        tabLayout.setupWithViewPager(view)

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val currentUser = snapshot.getValue(User ::class.java)
                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvatar.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@MessageActivity).load(currentUser?.imageURL).into(userAvatar)
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })


        //ALL BUTTON ACTIONS
        userAvatar.setOnClickListener{
            val intent = Intent(this, UserProfileActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //settings activity
        settingsButton.setOnClickListener{
            val intent = Intent(this, SettingsActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }
    inner class ViewPageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        val fragments : MutableList<Fragment> = ArrayList()
        val titles : MutableList<String> = ArrayList()

        override fun getCount(): Int {
            return fragments.size
        }

        override fun getItem(position: Int): Fragment {
            return fragments.get(position)

        }

        fun addFragment(fragment: Fragment, title : String){
            fragments.add(fragment)
            titles.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titles.get(position)
        }

    }
}