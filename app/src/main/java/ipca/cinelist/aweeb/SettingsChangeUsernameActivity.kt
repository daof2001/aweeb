package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView

class SettingsChangeUsernameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_expand_details2)

        /*BUTTONS*/
        val messageButton = findViewById<ImageView>(R.id.message_button7)
        val searchButton = findViewById<ImageView>(R.id.search_button7)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button7)
        val userAvi = findViewById<CircleImageView>(R.id.userIc7)
        val confirmButton = findViewById<Button>(R.id.button_confirmEmail)

        val newUsername = findViewById<EditText>(R.id.newUsername)
        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users").child(user?.uid.toString())

        confirmButton.setOnClickListener {
            val profileUpdates = userProfileChangeRequest {
                displayName = newUsername.text.toString()
            }

            user!!.updateProfile(profileUpdates)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Username successfully updated!!", Toast.LENGTH_SHORT).show()

                        reference.child("username").setValue(newUsername.text.toString())

                        val intent = Intent(this, UserProfileActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
        }

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var currentUser = snapshot.getValue(User ::class.java)

                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvi.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@SettingsChangeUsernameActivity).load(currentUser?.imageURL).into(userAvi)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        /*ALL UI BUttons*/
        userAvi.setOnClickListener{
            val intent = Intent(this, UserProfileActivity :: class.java)
            startActivity(intent)
        }

        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
        }

        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }
}