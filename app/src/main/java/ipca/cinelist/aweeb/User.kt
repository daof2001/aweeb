package ipca.cinelist.aweeb

import org.json.JSONObject

class User {

    var username : String? = null
    var id : String? = null
    var imageURL : String? = null
    var bio : String? = null
    var favoriteAnime : MutableList<Anime> = ArrayList()
    var seenAnime : MutableList<Anime> = ArrayList()
    //var reviews : MutableList<String> = ArrayList()

    constructor() {

    }

    constructor(username: String?, id: String?, imageURL: String?, bio: String?, favoriteAnime: MutableList<Anime>, seenAnime: MutableList<Anime>) {
        this.username = username
        this.id = id
        this.imageURL = imageURL
        this.bio = bio
        this.favoriteAnime = favoriteAnime
        this.seenAnime = seenAnime
        //this.reviews = reviews
    }


}